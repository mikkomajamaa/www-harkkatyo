"use strict";
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const session = require('express-session');
const auth = require('./routes/auth')

//constants
const PORT = 8080;
const HOST = '0.0.0.0';

//app
const app = express();
app.use(express.static('public'))
app.use(bodyParser.json());
app.use("/node_modules", express.static(__dirname + "/node_modules"));

var HiScores = require('./models/hiScores');
require('./models/Users');
require('./config/passport');

var routes = require('./routes/index');
app.use('/', routes);

//set up mongoose connection
var mongoose = require('mongoose');
var mongoDB = 'mongodb://mongo:27017';
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
