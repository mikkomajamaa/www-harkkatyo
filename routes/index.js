var express = require('express');
var router = express.Router();
var auth = require('../routes/auth')
//var highscore_controller = require('../controllers/highscoreController');

//GET request for list of all highscore items.
//router.get('/highscores', highscore_controller.highscore_list);

//POST request for creating a highscore.
//router.post('/highscores/create', auth.optional, highscore_controller.highscore_create_post);

router.use('/api', require('./api'));

module.exports = router;
