const express = require('express');
const router = express.Router();

router.use('/users', require('./users'));
router.use('/highscores', require('./highscores'))

module.exports = router;
