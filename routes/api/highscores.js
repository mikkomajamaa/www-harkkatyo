"use strict";
var async = require('async');
const mongoose = require('mongoose');
const Users = mongoose.model('Users');
const HiScores = mongoose.model('HiScore')
const router = require('express').Router();
const auth = require('../auth');

//display list of all highscores.
router.get('/', (req, res, next) => {
  HiScores
    .find({})
    .then(hiscores => {
      res.json(hiscores.map(highscore => highscore.formatHiScore))
    });
});

//post a new highscore
router.post('/create', auth.optional, (req, res, next) => {
  //post highscore on ones nickname if autorization token is found
  if (req.payload){
    const { payload: { id } } = req;
    //find the users nickname
    Users.findById(id)
      .then((user) => {
        //if user found is null, post the highscore as anonymous
        if(!user) {
          const body = req.body
          const hiscore = new HiScores({
            name: 'Anonymous',
            score: body.score
          })

          hiscore
          .save()
          .then(savedHiScore => {
            res.json(savedHiScore.formatHiScore)
          })
          .catch(error => {
            console.log(error)
          })
        } else {
          const body = req.body
          const hiscore = new HiScores({
            name: user.toAuthJSON().email,
            score: body.score
          })

          hiscore
          .save()
          .then(savedHiScore => {
            res.json(savedHiScore.formatHiScore)
          })
          .catch(error => {
            console.log(error)
          })
      }
      })
    //if user is not logged in post highscore as anonymous
    } else {
      const body = req.body
      const hiscore = new HiScores({
        name: 'Anonymous',
        score: body.score
      })

      hiscore
      .save()
      .then(savedHiScore => {
        res.json(savedHiScore.formatHiScore)
      })
      .catch(error => {
        console.log(error)
      })
    }
});

module.exports = router;
