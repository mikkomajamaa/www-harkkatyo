"use strict";
const mongoose = require('mongoose')

var Schema = mongoose.Schema;

var hiScoreSchema = new Schema({
  name: String,
  score: Number
})

hiScoreSchema.virtual('formatHiScore').get(function () {
  return ({
    id: this._id,
    name: this.name,
    score: this.score
  })
})

const HiScore = mongoose.model('HiScore', hiScoreSchema)

module.exports = HiScore
