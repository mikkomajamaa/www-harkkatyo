"use strict";
var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 300 },
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);
var platforms;
var cursors = null;
var score = 0;
var scoreText;
var bombs;
var localBestText;
var localBest;
var gameOverText;
var newGameText;
var player;
var golds;
var gameOver;
var bool = 1; //trigger to send highscore just once if game is ended by colliding with the bottom edge of the game
var firstGame = 1;
var firstLoad = 1;


function preload() {
  this.load.image("bg", "./assets/bg2.png")
  this.load.spritesheet("dwarf", "./assets/spritesheet.png",
    { frameWidth: 32, frameHeight: 32 })
  this.load.image("ground", "./assets/ground.png")
  this.load.image("gold", "assets/gold.png")
  this.load.image("bomb", "assets/asuna_by_vali233.png")
  if (!cursors) {
    cursors = this.input.keyboard.createCursorKeys();
  }
  if ((localStorage.getItem('token')) && (firstLoad)) {
      localStorage.removeItem('token');
      firstLoad = 0;
  }
  bool = 1;
}

function create() {
    //add background
    this.add.tileSprite(0, 0, 0, 0, "bg");

    //add platforms and set them to correct places
    platforms = this.physics.add.staticGroup();
    platforms.create(400, 568, "ground").setScale(2).refreshBody();
    platforms.create(320, 290, "ground").setScale(1.5).refreshBody();
    platforms.create(600, 400, 'ground');
    platforms.create(80, 250, 'ground');
    platforms.create(700, 220, 'ground');

    //add player
    player = this.physics.add.sprite(350, 450, 'dwarf');
    player.setBounce(0.2);
    player.setCollideWorldBounds(true);
    this.physics.add.collider(player, platforms);

    //create animations when game is loaded the first time
    if (firstGame) {
      this.anims.create({
          key: 'left',
          frames: this.anims.generateFrameNumbers('dwarf', { start: 50, end: 54 }),
          frameRate: 10,
          repeat: -1
      });

      this.anims.create({
          key: 'turn',
          frames: [ { key: 'dwarf', frame: 4 } ],
          frameRate: 20
      });

      this.anims.create({
          key: 'right',
          frames: this.anims.generateFrameNumbers('dwarf', { start: 0, end: 4 }),
          frameRate: 10,
          repeat: -1
      });

      this.anims.create({
          key: 'dead',
          frames: this.anims.generateFrameNumbers('dwarf', { start: 90, end: 98 }),
          frameRate: 10,
          repeat: -1
      });

      firstGame = 0;
    }

   if (!cursors){
        cursors = this.input.keyboard.createCursorKeys();
        var xCoordinates = [270, 82, 222, 92];
        var yCoordinates = [0, 0, 10, 10];
        var i = 0;
      }

  //add the the collectable goods
  golds = this.physics.add.group({
     key: 'gold',
     repeat: 4,
     setXY: { x: 95, y: 0, stepX: 160},
   });

   golds.children.iterate(function (child) {
       child.setBounceY(Phaser.Math.FloatBetween(0.3, 0.6));
   });

   this.physics.add.collider(golds, platforms);
   this.physics.add.overlap(player, golds, collectGold, null, this);

   //collectGold function to handle player colliding with the collectable goods
   function collectGold (player, gold) {
       gold.disableBody(true, true);
       score += 10;
       scoreText.setText('score: ' + score);
       if (score > topscore) {
         localBestText.setText('')
         var string = 'local best: ' + score
         localBestText = this.add.text(800 - string.length*20, 16, string, { fontSize: '32px', fill: '#000' })
       }

       //iterate more collectable goods if none is left
       if (golds.countActive(true) === 0){
         golds.children.iterate(function (child) {
            child.enableBody(true, child.x, 0, true, true);
        });

        //generate an enemy after all the collectable goods are collected
        var x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);
        var i = 2;
        var bomb = bombs.create(x, 16, 'bomb');
        var xVelocity = 100*((i++)/4)
        bomb.setBounce(1);
        bomb.setCollideWorldBounds(true);
        bomb.setVelocity(xVelocity, 20);
        bomb.allowGravity = false;
    }
   }

   //add enemy and set its configuration
   bombs = this.physics.add.group();
   this.physics.add.collider(bombs, platforms);
   this.physics.add.collider(player, bombs, hitBomb, null, this);
   //hitBomb function to handle player hitting an enemy
   function hitBomb (player, bomb) {
     this.physics.pause();
     player.setTint(0xff0000);
     player.anims.play('dead');
     gameOverText = this.add.text(316, 316, 'GAME OVER', { fontSize: '32px', fill: '#000' });
     gameOver = true;
     if (gameOver) {
       //get an authorization token from local storage
       if (localStorage.getItem('token')) {
         try {
           var token = localStorage.getItem('token')
         } catch(e) {
           localStorage.removeItem('token');
         }
       }

       //create a score object
       var scoreObject = {
         name : 'randomUser',
         score : score
       }

       //post highscore
       if ((token) && (score > 0)) {
         //create an autorization token
         const AuthStr = 'Token '.concat(token);
         axios({
           method: "post",
           url: '/api/highscores/create',
           data: scoreObject,
           headers: {authorization: AuthStr}
         })
     } else if (score > 0){
         axios({
           method: "post",
           url: '/api/highscores/create',
           data: scoreObject,
         })
       }

       //create a local best score
       if ((localStorage.getItem('topscore')) && (score > 0)) {
         try {
           var topscore = JSON.parse(localStorage.getItem('topscore'))[0].score;
           if (score > topscore) {
             const parsed = JSON.stringify([scoreObject]);
             localStorage.setItem('topscore', parsed)

           }
         } catch(e) {
           localStorage.removeItem('topscore');
         }
       } else if (score > 0){
           const parsed = JSON.stringify([scoreObject]);
           localStorage.setItem('topscore', parsed)
           localBestText.setText('local best: ' + score)
       }


     }
   }

   //create a new game 'button'
   newGameText = this.add.text(16, 552, 'NEW GAME', { fontSize: '32px', fill: '#000' });
   newGameText.setInteractive({ useHandCursor: true })
   .on('pointerdown', () => {
     this.scene.restart();
     score = 0;
   })
   .on('pointerover', () => {newGameText.setStyle({fill: '#ff0'})})
   .on('pointerout', () => {newGameText.setStyle({fill: '#00'})});

   //get local best score from the local storage
   if (localStorage.getItem('topscore')) {
     try {
       var topscore = JSON.parse(localStorage.getItem('topscore'))[0].score;
     } catch(e) {
       localStorage.removeItem('topscore');
     }
   } else {
     var topscore = 0;
   }

   //add score texts
   scoreText = this.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#000' });
   var string = 'local best: ' + topscore
   localBestText = this.add.text(800 - string.length*20, 16, string, { fontSize: '32px', fill: '#000' })

}

function update() {
  //player hits the bottom edge of the game
  if (player.y == 584) {
    this.physics.pause();
    player.setTint(0xff0000);
    player.anims.play('dead');

    //only post highscore once
    if (bool){
      if (localStorage.getItem('token')) {
        try {
          var token = localStorage.getItem('token')
        } catch(e) {
          localStorage.removeItem('token');
        }
      }

      var scoreObject = {
        name : 'randomUser',
        score : score
      }

      //post highscore
      if ((token) && (score > 0)) {
        const AuthStr = 'Token '.concat(token);
        axios({
          method: "post",
          url: '/api/highscores/create',
          data: scoreObject,
          headers: {authorization: AuthStr}
        })
      } else if (score > 0) {
        axios({
          method: "post",
          url: '/api/highscores/create',
          data: scoreObject,
        })
      }

      //set the trigger to post highscore just once to false
      bool = 0;

      //create a local best score
      if ((localStorage.getItem('topscore')) && (score > 0)) {
        try {
          var topscore = JSON.parse(localStorage.getItem('topscore'))[0].score;
          if (score > topscore) {
            const parsed = JSON.stringify([scoreObject]);
            localStorage.setItem('topscore', parsed)
          }
        } catch(e) {
          localStorage.removeItem('topscore');
        }
      } else if (score > 0) {
          const parsed = JSON.stringify([scoreObject]);
          localStorage.setItem('topscore', parsed)
          localBestText.setText('local best: ' + score)
      }
    }

    gameOverText = this.add.text(316, 316, 'GAME OVER', { fontSize: '32px', fill: '#000' });
  }

  if (cursors.left.isDown) {
    player.body.velocity.x = -160;
    player.anims.play('left', true);
  } else if (cursors.right.isDown) {
    player.setVelocityX(160);
    player.anims.play('right', true);
  } else {
      player.setVelocityX(0);
  }

  if (cursors.up.isDown && player.body.touching.down)
  {
      player.setVelocityY(-330);
  }
}
