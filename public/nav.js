"use strict";

//event listener for the responsive navigation bar
document.addEventListener('DOMContentLoaded', function() {
 var elems = document.querySelectorAll('.sidenav');
 var instances = M.Sidenav.init(elems, {});
});
