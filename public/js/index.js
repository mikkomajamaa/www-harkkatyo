"use strict";
new Vue({
  el: '.js-instance',
  name: 'vue-instance',

  data() {
    return {
      highscores: [{}],
      topscore: null,
      username: null,
      password: null,
      newUsername: null,
      newPassword: null,
      newPassword2: null,
      token: null,
      warning: null,
      loginError: null,
      pwsDontMatch: 0,
      showSuccesfulSignUp: 0,
      loginStatus: 1,
      loggedInUser: null
    };
  },

  mounted() {
    //get the local best score
    if (localStorage.getItem('topscore')) {
      try {
        this.topscore = JSON.parse(localStorage.getItem('topscore'));
      } catch(e) {
        localStorage.removeItem('topscore');
      }
    }

    //remove the old token
    if (localStorage.getItem('token')) {
      try {
        localStorage.removeItem('token');
      } catch(e) {
        localStorage.removeItem('token');
      }
    }

    //get all the highscores
    axios.get('/api/highscores')
       .then(res => {
         this.highscores = res.data;
         this.highscores.sort(function (a, b) {
          return b.score - a.score;
        });

        var i = 1;
        this.highscores.map((hi)=> {hi.i = i++})
       })
       .catch(function (error) {
         console.log(error)
       })
  },

  methods: {
    //login status to handle rendering right components on the html file
    changeLoginStatus() {
        if (this.loginStatus == 1) {
          this.loginStatus = 2;
          this.loginError = 0;
        } else if (this.loginStatus == 2){
          this.loginStatus = 1;
        }
        this.pwsDontMatch = 0;
        this.showSuccesfulSignUp = 0;
    },

    //sace the local best score
    saveTopScore() {
      const parsed = JSON.stringify([{'name': 'mikko', 'score': score}]);
      localStorage.setItem('topscore', parsed)
    },

    //login to save highscores under ones own nickname
    login() {
      const userObject = {user: {
        email: this.username,
        password: this.password}
      }

      //get authorization token
      axios({
        method: "post",
        url: "/api/users/login",
        data: userObject
      }).then(response =>{
        this.token = response.data.user.token
        this.loginError = null;
        this.loginStatus = 3;
        this.username = null;
        this.password = null;
        this.loggedInUser = response.data.user.email;

        //save the authorization token to local storage to access it through the Phaser component
        localStorage.setItem('token', response.data.user.token)
      })
      .catch((error) => {
        this.loginError = 'Invalid username or password.';
      });
    },

    logout() {
      //remove the authorization toke on logout
      localStorage.removeItem('token');
      this.loginStatus = 1;
    },

    createAccount() {
      if (!(this.newPassword == this.newPassword2)) {
        this.pwsDontMatch = 1;
        return;
      }

      const userObject = {
        user: {
          email: this.newUsername,
          password: this.newPassword
        }
      }

      axios({
        method: "post",
        url: "/api/users",
        data: userObject
      }).then(response =>{
        this.showSuccesfulSignUp = 1;
        this.newUsername = null;
        this.newPassword = null;
        this.newPassword2 = null;
      });
    },
  },
});
